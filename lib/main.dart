import 'package:flutter/material.dart';
import 'package:teccampareejbuthainafinal/core/presentation/app_base.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',


      home: AppBase(),
    );
  }
}


